import express from "express";
import userRouter from "./userRouter.js";
import productRouter from "./productRouter.js";
import likeRouter from "./likeRouter.js";
import rateRouter from "./rateRouter.js";
import orderRouter from "./orderRoute.js";

const rootRouter = express.Router();

rootRouter.use("/user", userRouter);

rootRouter.use("/product", productRouter);

rootRouter.use("/like", likeRouter);
rootRouter.use("/rate", rateRouter);
rootRouter.use("/order", orderRouter);

export default rootRouter;

// localhost:8080/api/user/get-nguoi-dung
// create-nguoi-dung
