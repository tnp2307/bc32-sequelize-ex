import express from "express";
import { getRateListByRes, getRateListByUser, postRate } from "../controllers/rateController.js";
const rateRouter = express.Router();

rateRouter.post("/post-rate/:user_id/:res_id", postRate);
rateRouter.get("/get-rate-by-user/:user_id",getRateListByUser );
rateRouter.get("/get-rate-by-res/:res_id", getRateListByRes);

export default rateRouter;
