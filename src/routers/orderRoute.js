import express from "express";
import { postOrder } from "../controllers/orderController.js";

const orderRouter = express.Router();
orderRouter.post("/post-order/:user_id/:food_id", postOrder);

export default orderRouter;
