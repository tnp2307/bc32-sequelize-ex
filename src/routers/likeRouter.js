import express from "express";
import {
  getLikeListByRes,
  getLikeListByUser,
  postLike,
  postUnLike,
} from "../controllers/likeController.js";

const likeRouter = express.Router();

likeRouter.post("/post-like/:user_id/:res_id", postLike);
likeRouter.post("/post-unlike/:user_id/:res_id", postUnLike);
likeRouter.get("/get-like-list-by-user/:user_id", getLikeListByUser);
likeRouter.get("/get-like-list-by-res/:res_id", getLikeListByRes);

export default likeRouter;
