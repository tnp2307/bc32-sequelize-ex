import {
  getNguoiDung,
  createNguoiDung,
  updateNguoiDung,
  removeNguoiDung,
  getUserPage,
} from "../controllers/userController.js";

import express from "express";

const userRouter = express.Router();

//__dirname : trả về đường dẫn file đang đứng
//process.cwd(): trả về đường dẫn gốc

import multer from "multer";

const storage = multer.diskStorage({
  destination: process.cwd() + " /public/img",
  filename: (req, file, callback) => {
    let newName = new Date().getTime() + "_" + file.originalname;

    callback(null, newName);
  },
});
const upload = multer({
  //   dest: process.cwd() + "/public/img", // destination: khai báo đường dẫn lưu tài nguyên ở BE
  storage: storage,
});

userRouter.post("/upload", upload.single("file"), (req, res) => {
  let file = req.body;
  res.send("upload successfull");
});

// File system FS
import fs from "fs";

userRouter.post("/base64", (req, res) => {
  // fs.writeFile(process.cwd() + "/test.txt", "Hello world", () => { });
  // fs.appendFile(process.cwd() + "/test.txt", " abc", () => { })
  let file = req.file;
  fs.readFile(process.cwd() + "/public/img/" + file.filename, (err, data) => {
      // parse base64

      let newName = `data:${file.mimetype};base64,${Buffer.from(data).toString("base64")}`;

      res.send(newName);
  })


  // fs.readFile();
  // fs.copyFile();
  // fs.unlink();
});

userRouter.get("/get-nguoi-dung", getNguoiDung);
//userRouter.get("/get-nguoi-dung-by-id/:food_id", getNguoiDungId)
userRouter.post("/create-nguoi-dung", createNguoiDung);
userRouter.put("/update-nguoi-dung/:user_id", updateNguoiDung);
userRouter.delete("/remove-nguoi-dung/:user_id", removeNguoiDung);

//Pagination
userRouter.get("/get-user-page/:page/:pageSize", getUserPage);

export default userRouter;
