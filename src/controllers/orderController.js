import { Sequelize } from "sequelize";
import initModels from "../models/init-models.js";
import { errorCode, failCode, successCode } from "../config/response.js";
import sequelize from "../models/index.js";

const models = initModels(sequelize);
const Op = Sequelize.Op;

const postOrder = async (req, res) => {
  try {
    let { user_id, food_id } = req.params;
    let { amount, code, arr_sub_id, order_id } = req.body;
    let newData = {
      user_id,
      food_id,
      order_id,
      amount,
      code,
      arr_sub_id,
    };
    await models.order.create(newData);
    successCode(res, newData, "Order success");
  } catch (error) {
    failCode(res, "no data sent", "Order fail");
  }
};

export { postOrder };
