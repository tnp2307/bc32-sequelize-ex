import { Sequelize } from "sequelize";
import initModels from "../models/init-models.js";
import { errorCode, failCode, successCode } from "../config/response.js";
import sequelize from "../models/index.js";

const models = initModels(sequelize);
const Op = Sequelize.Op;

const postLike = async (req, res) => {
  try {
    let { user_id, res_id } = req.params;
    let newData = {
      user_id,
      res_id,
      date_like: Date.now(),
    };
    await models.like_res.create(newData);
    res.status(200).send("Like thành công");
    // successCode(res,data, "Like thanh cong");
  } catch (error) {
    // res.status(500).send(error.message)
    errorCode(res, error);
  }
};
const postUnLike = async (req, res) => {
  try {
    let { user_id, res_id } = req.params;
    let checkLike = await models.like_res.findAll({
      where: { user_id, res_id },
    });
    if (checkLike.length > 0) {
      await models.like_res.destroy({ where: { user_id, res_id } });
      successCode(res, "", "Unlike thành công");
    } else {
      failCode(res, "", "Nha hang da unlike/ chua duoc like");
    }
  } catch (error) {
    errorCode(res, error);
  }
};
const getLikeListByUser = async (req, res) => {
  try {
    let { user_id } = req.params;
    let data = await models.user.findAll({
      where: { user_id },
      include: "res_id_restaurants",
    });
    successCode(res, data, "lay danh sach thanh cong");
  } catch (error) {
    errorCode(res, "", "Lay danh sach that bai");
  }
};
const getLikeListByRes = async (req, res) => {
  try {
    let { res_id } = req.params;
    let data = await models.restaurant.findAll({
      where: { res_id },
      include: "user_id_users",
    });
    successCode(res, data, "lay danh sach thanh cong");
  } catch (error) {
    errorCode(res, "", "Lay danh sach that bai");
  }
};
export { postLike, postUnLike, getLikeListByUser,getLikeListByRes };
