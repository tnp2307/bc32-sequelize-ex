import { Sequelize } from "sequelize";
import initModels from "../models/init-models.js";
import { errorCode, failCode, successCode } from "../config/response.js";
import sequelize from "../models/index.js";

const models = initModels(sequelize);
const Op = Sequelize.Op;

const postRate = async (req, res) => {
  // res.status(200).send("ok")
  try {
    let { user_id, res_id } = req.params;
    let { amount } = req.body;

    let newData = {
      user_id,
      res_id,
      amount,
      date_rate: Date.now(),
    };
    const checkRate = await models.rate_res.findAll({
      where: { user_id, res_id },
    });
    if (checkRate.length > 0) {
      await models.rate_res.update(newData, { where: { user_id, res_id } });
    } else {
      await models.rate_res.create(newData);
    }
    // res.status(200).send("ok")
    successCode(res, newData, "Rate thanh cong");
  } catch (error) {
    errorCode(res, error, "Fail");
  }
};
const getRateListByUser = async (req, res) => {
  try {
    let { user_id } = req.params;
    let data = await models.user.findAll({
      where: { user_id },
      include: "res_id_restaurant_rate_res",
    });
    successCode(res, data, "lay danh sach thanh cong");
  } catch (error) {
    errorCode(res, "", "Lay danh sach that bai");
  }
};
const getRateListByRes = async (req, res) => {
  try {
    let { res_id } = req.params;
    let data = await models.restaurant.findAll({
      where: { res_id },
      include: "user_id_user_rate_res",
    });
    successCode(res, data, "lay danh sach thanh cong");
  } catch (error) {
    errorCode(res, "", "Lay danh sach that bai");
  }
};

export { postRate, getRateListByUser, getRateListByRes };
